package com.demica.scf.pe.e2e;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan(basePackages = {"com.demica.scf.mp.model.domain"})
@SpringBootApplication
public class E2EApplication {
    private static final Logger logger = LogManager.getLogger(E2EApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(E2EApplication.class, args);
        logger.info("E2E testing application has been started successfully!");
    }
}
