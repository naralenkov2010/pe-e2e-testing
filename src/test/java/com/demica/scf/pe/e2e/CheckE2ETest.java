package com.demica.scf.pe.e2e;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.util.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.jms.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class CheckE2ETest extends BasicTest {
    @Autowired
    DataSource dataSource;

    @PersistenceContext
    private EntityManager em;

    private static final Logger logger = LogManager.getLogger(CheckE2ETest.class);

    @Value("${queue.all-invoices}")
    private String allInvoicesQueue;

    @Value("${queue.unapproved-invoices}")
    private String unapprovedInvoicesQueue;

    @Value("${queue.approved-invoices}")
    private String approvedInvoicesQueue;

    @Value("${queue.funded-invoices}")
    private String fundedInvoicesQueue;

    @Value("${spring.activemq.broker-url}")
    private String activeMqURL;

    @Value("${spring.activemq.user}")
    private String activeMqUser;

    @Value("${spring.activemq.password}")
    private String activeMqPassword;

    private final Long DELAY_MS = 1000L;

    private final String UPDATE_PORTFOLIO_STATUS_SQL = "UPDATE prtflio set prtflio_stat = 10 WHERE long_name like '%pig1%'";

    private static final String INSERT_TEST_INVOICE_STR_QUERY = "INSERT INTO str (mfst, chng_by, date_mod, vsn, audit, arch, stat, ownr_lbl, is_prchsd, " +
            "inv_type, inv_stat, inv_deflt_stat, otc_mfst, prtflio_mfst, seq_num, prev_seq_num, actn, lvl, curr_mfst, amnt, maty_date, issue_date, " +
            "posting_date, rate_date, is_elig, elig_flag, enty_prog_mfst, src_enty_prog_mfst, enty_mfst, trust_mfst, trust_code, cntrprty_mfst, " +
            "cntrprty_code, cntrprty_name, prchs_price, prchs_rate, po_box, addr_line1, addr_line2, addr_line3, addr_line4, cntry_mfst, cntry_code, " +
            "state_mfst, state_code, city_mfst, city_code, post_code, data_src_mfst, outstdng_prin, xpctd_pay_date, dltion_amnt, same_prd_dltion_amnt, " +
            "latr_prd_dltion_amnt, buy_back_stat, lim_amnt, date_from, date_to, event_date, deal_mfst, pndng_actn, usr_def_1, usr_def_2, usr_def_3, " +
            "usr_def_4, usr_def_5, usr_def_6, usr_def_7, usr_def_8, usr_def_9, usr_def_10, usr_def_11, usr_def_12, usr_def_13, usr_def_14, usr_def_15, " +
            "usr_def_16, usr_def_17, usr_def_18, usr_def_19, usr_def_20, usr_def_21, usr_def_22, usr_def_23, usr_def_24, usr_def_25, usr_def_26, usr_def_27, " +
            "usr_def_28, usr_def_29, usr_def_30, usr_def_1_mfst, usr_def_2_mfst, usr_def_3_mfst, usr_def_4_mfst, usr_def_5_mfst, usr_def_6_mfst, " +
            "usr_def_7_mfst, usr_def_8_mfst, usr_def_9_mfst, usr_def_10_mfst, usr_def_11_mfst, usr_def_12_mfst, usr_def_13_mfst, usr_def_14_mfst, " +
            "usr_def_15_mfst, usr_def_16_mfst, usr_def_17_mfst, usr_def_18_mfst, usr_def_19_mfst, usr_def_20_mfst, usr_def_21_mfst, usr_def_22_mfst, " +
            "usr_def_23_mfst, usr_def_24_mfst, usr_def_25_mfst, usr_def_26_mfst, usr_def_27_mfst, usr_def_28_mfst, usr_def_29_mfst, usr_def_30_mfst, " +
            "PWDD_amnt, SSDD_amnt, CID_amnt, GCD_amnt, FCD_amnt, LCD_amnt, LFD_amnt) " +

            "VALUES(?, 8, getdate(), 1, 0, 0, 1, 999001158, 0, 1, 1, 0, NULL, 150002007692548, ?, ?, NULL, 0, 10090, ?, '2019-04-13 00:00:00.000', " +
            "'2019-04-12 00:00:00.000', '2019-05-23 00:00:00.000', NULL, 1, 0, NULL, NULL, 150002007692528, NULL, NULL, 150002007692528, 'test1', NULL, " +
            "0.0000, 0.0000000000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 150002007692450, ?, '2019-05-24 00:00:00.000', " +
            "0.0000, 0.0000, NULL, 0, 0.0000, '2019-05-23 10:27:20.000', '5000-01-01 00:00:00.000', '2019-05-23 10:27:20.000', NULL, 0, 'test', '0', '1', " +
            "NULL, NULL, '1', '0', '1', '1', NULL, ?, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, " +
            "NULL, NULL, NULL, 150002007692447, NULL, NULL, NULL, NULL, 11933, 11936, 11941, 11945, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, " +
            "NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000)";

    private static final String INSERT_TEST_INVOICE_STR_HDR_QUERY = "INSERT INTO str_hdr (mfst, chng_by, date_mod, vsn, audit, arch, stat, ownr_lbl, " +
            "is_prchsd, inv_id, inv_stat, inv_type, crnt_seq_num, inpt_file_fmt_mfst, outstdng_prin, xpctd_pay_date, orig_amnt, orig_seq_num, crnt_lvl, inv_stat_chng) " +
            "VALUES(?, 8, getdate(), 1, 0, 0, 1, 999001158, 0, ?, 1, NULL, ?, 150002007692549, ?, '2019-05-24 00:00:00.000', ?, ?, 0, NULL)";

    private Connection activeMqConnection;
    private Session activeMqSession;
    private Destination destinationAll;
    private Destination destinationApproved;
    private MessageProducer jmsProducer;
    private MessageConsumer jmsConsumer;

    @Before
    public void initJMS() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(activeMqURL);
        connectionFactory.setUserName(activeMqUser);
        connectionFactory.setPassword(activeMqPassword);
        try {
            /**
             * Update DB according to changes, that should be made by Preprocessor
             */
            java.sql.Connection dbConn = dataSource.getConnection();
            PreparedStatement updateStatusPs = dbConn.prepareStatement(UPDATE_PORTFOLIO_STATUS_SQL);
            updateStatusPs.executeUpdate();

            /**
             * Configure ActiveMQ producer and consumer
             */
            activeMqConnection = connectionFactory.createConnection();
            activeMqConnection.start();
            activeMqSession = activeMqConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create producer for All Invoices queue
            destinationAll = activeMqSession.createQueue(allInvoicesQueue);
            jmsProducer = activeMqSession.createProducer(destinationAll);
            jmsProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            // Create consumer for Approved Invoices queue
            destinationApproved = activeMqSession.createQueue(approvedInvoicesQueue);
            jmsConsumer = activeMqSession.createConsumer(destinationApproved);
        } catch (Exception e) {
            logger.error(e.getStackTrace());
        }
    }

    @Test
    public void checkFileProcessing() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream("test-message.json");
        String messageStr = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
        TextMessage inputMessage = activeMqSession.createTextMessage(messageStr);
        jmsProducer.send(inputMessage);
    }

    @After
    public void closeActiveMQSession() {
        try {
            activeMqSession.rollback();
            activeMqSession.close();
            activeMqConnection.close();
        } catch (JMSException e) {
            logger.error(e.getStackTrace());
        }
    }
}
