package com.demica.scf.pe.e2e;

import com.demica.scf.pe.util.RestClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.net.URI;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class CheckServicesTest extends BasicTest {

    private static final Logger logger = LogManager.getLogger(CheckServicesTest.class);

    @Value("${env.host}")
    private String host;

    @Value("${env.namespace}")
    private String namespace;

    @Test
    public void checkProgramEngine() {
        URI programEngineURI = RestClientBuilder.createURI(host + namespace, "/engine", "/hello");
        try {
            var response = RestClientBuilder.createGetRequest(programEngineURI, null, String.class);
            Assert.assertEquals(response, "Hello from pe-program-engine service on test");
        } catch (Exception e) {
            logger.error(e.getStackTrace());
        }
    }

    @Test
    public void checkTransferService() {
        URI transferServiceURI = RestClientBuilder.createURI(host + namespace, "/transfer", "");
        try {
            var response = RestClientBuilder.createGetRequest(transferServiceURI, null, String.class);
            Assert.assertEquals(response, "Transfer Service is up and running");
        } catch (Exception e) {
            logger.error(e.getStackTrace());
        }
    }

    @Test
    public void checkInvoiceService() {
        URI invoiceServiceURI = RestClientBuilder.createURI(host + namespace, "/invoice", "");
        try {
            var response = RestClientBuilder.createGetRequest(invoiceServiceURI, null, String.class);
            Assert.assertEquals(response, "Invoice Service is up and running");
        } catch (Exception e) {
            logger.error(e.getStackTrace());
        }
    }

    @Test
    public void checkDealingService() {
        URI dealingServiceURI = RestClientBuilder.createURI(host + namespace, "/dealing", "");
        try {
            var response = RestClientBuilder.createGetRequest(dealingServiceURI, null, String.class);
            Assert.assertEquals(response, "Dealing Service is up and running");
        } catch (Exception e) {
            logger.error(e.getStackTrace());
        }
    }

    @Test
    public void checkWorkflowService() {
        URI workflowServiceURI = RestClientBuilder.createURI(host + namespace, "/workflow", "");
        try {
            var response = RestClientBuilder.createGetRequest(workflowServiceURI, null, String.class);
            Assert.assertEquals(response, "Workflow Service is up and running");
        } catch (Exception e) {
            logger.error(e.getStackTrace());
        }
    }

    @Test
    public void checkPortfolioService() {
        URI portfolioServiceURI = RestClientBuilder.createURI(host + namespace, "/portfolio", "");
        try {
            var response = RestClientBuilder.createGetRequest(portfolioServiceURI, null, String.class);
            Assert.assertEquals(response, "Portfolio Service is up and running");
        } catch (Exception e) {
            logger.error(e.getStackTrace());
        }
    }
}
