package com.demica.scf.pe.e2e;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.jms.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class CheckActiveMQTest extends BasicTest {
    private static final Logger logger = LogManager.getLogger(CheckActiveMQTest.class);

    private final String TEST_QUEUE = "Test Queue";
    private final String MSG_TEXT = "Test message to MP ActiveMQ";
    private final Long DELAY_MS = 1000L;

    private Connection activeMqConnection;
    private Session activeMqSession;
    private Destination jmsDestination;
    private MessageProducer jmsProducer;
    private MessageConsumer jmsConsumer;

    @Value("${spring.activemq.broker-url}")
    private String activeMqURL;

    @Value("${spring.activemq.user}")
    private String activeMqUser;

    @Value("${spring.activemq.password}")
    private String activeMqPassword;

    @Before
    public void initActiveMQProducer() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(activeMqURL);
        connectionFactory.setUserName(activeMqUser);
        connectionFactory.setPassword(activeMqPassword);
        try {
            activeMqConnection = connectionFactory.createConnection();
            activeMqConnection.start();
            activeMqSession = activeMqConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            jmsDestination = activeMqSession.createQueue(TEST_QUEUE);
            jmsProducer = activeMqSession.createProducer(jmsDestination);
            jmsProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            jmsConsumer = activeMqSession.createConsumer(jmsDestination);
        } catch (Exception e) {
            logger.error(e.getStackTrace());
        }
    }

    @Test
    public void checkActiveMqAccess() {
        try {
            TextMessage inputMessage = activeMqSession.createTextMessage(MSG_TEXT);
            jmsProducer.send(inputMessage);

            Message outputMessage = jmsConsumer.receive(DELAY_MS);
            assertTrue(outputMessage instanceof TextMessage);
            TextMessage textMessage = (TextMessage) outputMessage;
            assertEquals(textMessage.getText(), MSG_TEXT);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @After
    public void closeActiveMQSession() {
        try {
            activeMqSession.rollback();
            activeMqSession.close();
            activeMqConnection.close();
        } catch (JMSException e) {
            logger.error(e.getStackTrace());
        }
    }
}
